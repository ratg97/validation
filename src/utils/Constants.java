package utils;

public final class Constants {

    private Constants() {
    }
    public final static int OPTION_LOOP = -1;
    public static final int OPTION1 = 1;
    public static final String OPTION1S = "Jugar";
    public static final int OPTION2 = 50450;
    public static final String OPTION2S = "Instrucciones";
    public static final int OPTION3 = 3;
    public static final String OPTION3S = "Salir";

    public static final String SEPARATOR_MESSAGE = "----------------------------------------------------";
    public static final String ASK_MESSAGE = "\n--> Introduce tu opción: ";
    public static final String ASK_TITLE = "Menú";
    public static final String ERROR_MESSAGE = "Introduce un valor correcto";
    public static final String ERROR_TITLE = "Error";
    public static final String OPTION_UKNOWN = SEPARATOR_MESSAGE + "\nLa opción elegida no tiene funcionalidades asignadas";

}
