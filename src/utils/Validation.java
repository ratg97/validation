package utils;

import java.util.Scanner;
import javax.swing.JOptionPane;

public final class Validation {

    private Validation() {
    }

    /**
     * Método que nos devuelve la opción elegida por el usuario.
     *
     * @param graphics True modo gráfico. False por línea de comandos.
     * @param positionVertical True vertical. False horizontal.
     * @param args Todos las opciones válidas.
     * @return La opción elegida.
     */
    public static int getNumber(boolean graphics, boolean positionVertical, int... args) {
        int num = Constants.OPTION_LOOP;
        boolean answerRight;
        StringBuilder message = getFullMessage(args, positionVertical);
        String askMessage = ((Constants.ASK_MESSAGE.isEmpty()) ? "\n" : Constants.ASK_MESSAGE);
        do {
            try {
                do {
                    if (graphics) {
                        String numS = JOptionPane.showInputDialog(null, message + askMessage, Constants.ASK_TITLE, JOptionPane.INFORMATION_MESSAGE);
                        num = Integer.parseInt(numS);
                    } else {
                        System.out.print(message + askMessage);
                        num = new Scanner(System.in).nextInt();
                    }
                    if (!checkNum(args, num)) {
                        sendErrorMessage(graphics);
                    }
                } while (!checkNum(args, num));
                answerRight = true;
            } catch (final Exception e) {
                answerRight = false;
                sendErrorMessage(graphics);
            }
        } while (!answerRight);
        if (!graphics) {
            System.out.println(Constants.SEPARATOR_MESSAGE);
        }
        return num;
    }

    private static boolean checkNum(int[] args, int num) {
        int i = 0;
        int len = args.length;
        boolean found = false;
        while (i < len && !found) {
            if (args[i] == num) {
                found = true;
            }
            i++;
        }
        return found;
    }

    private static StringBuilder getFullMessage(int[] args, boolean positionVertical) {
        int len = args.length;
        StringBuilder message = new StringBuilder();
        message.append(Constants.SEPARATOR_MESSAGE + "\n");
        for (int i = 0; i < len; i++) {
            message.append("[");
            message.append(args[i]);
            message.append("] ");
            message.append(getMessage(args[i]));
            if (positionVertical) {
                message.append("\n");
            } else {
                message.append(" ");
            }
        }
        if (positionVertical) {
            message.append(Constants.SEPARATOR_MESSAGE);
        } else {
            message.append("\n" + Constants.SEPARATOR_MESSAGE);
        }
        return message;
    }

    private static String getMessage(int op) {
        switch (op) {
            case Constants.OPTION1:
                return Constants.OPTION1S;
            case Constants.OPTION2:
                return Constants.OPTION2S;
            case Constants.OPTION3:
                return Constants.OPTION3S;
            default:
                return "";
        }
    }

    private static void sendErrorMessage(boolean graphics) {
        if (graphics) {
            JOptionPane.showMessageDialog(null, Constants.ERROR_MESSAGE, Constants.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
        } else {
            System.out.println(Constants.SEPARATOR_MESSAGE + "\n" + Constants.ERROR_MESSAGE);
        }
    }
}
