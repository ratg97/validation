package main;

import utils.Constants;
import utils.Validation;

public class Main {

    public static void main(String... args) {
        int num;
        do {
            num = Validation.getNumber(false, true, Constants.OPTION1, Constants.OPTION2, Constants.OPTION3);
            switch (num) {
                case Constants.OPTION1:
                    break;
                case Constants.OPTION2:
                    break;
                case Constants.OPTION3:
                    break;
                default:
                    System.out.println(Constants.OPTION_UKNOWN);
                    break;
            }
        } while (num != Constants.OPTION3);
    }
}
